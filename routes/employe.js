const express = require("express");
const router = express.Router();
const { routeHelper } = require("../utils/simpleRouteHelper");
const { employeValidator } = require("../validator");

const { requireSignin, isAdmin, isAuth } = require("../controllers/auth");

const {
  create,
  read,
  remove,
  update,
  list,
  removeMany,
  byId,
} = require("../controllers/employe");

module.exports = routeHelper(
  "employe",
  "employes",
  create,
  read,
  remove,
  update,
  list,
  byId,
  employeValidator,
  router,
  () => {
    router.delete(
      "/employes/:userId",
      requireSignin,
      isAuth,
      isAdmin,
      removeMany
    );

    router.get("/employes/", list);
  }
);
