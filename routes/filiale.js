const express = require("express");
const router = express.Router();
const { routeHelper } = require("../utils/simpleRouteHelper");
const { filialeValidator } = require("../validator");

const {
  requireSignin,
  isAdmin,
  isAuth,
} = require("../controllers/auth");

const {
  create,
  read,
  remove,
  update,
  list,
  removeMany,
  byId,
} = require("../controllers/filiale");

module.exports = routeHelper(
  "filiale",
  "filiales",
  create,
  read,
  remove,
  update,
  list,
  byId,
  filialeValidator,
  router,
  () => {
    router.delete(
      "/filiales/:userId",
      requireSignin,
      isAuth,
      isAdmin,
      removeMany
    );

    router.get("/filiales/", list);
  }
);
