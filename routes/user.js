const express = require("express");
const router = express.Router();
const { routeHelper } = require("../utils/simpleRouteHelper");
const { userUpdateProfileValidator } = require("../validator");
const { requireSignin, isAuth, isAdmin } = require("../controllers/auth");
const { validator } = require("../validator");

const {
  userById,
  read,
  remove,
  info,
  update,
  list,
} = require("../controllers/user");

module.exports = routeHelper(
  "user",
  "users",
  undefined,
  read,
  remove,
  update,
  list,
  userById,
  userUpdateProfileValidator,
  router,
  () => {
    router.get("/users/", list);

    router.param("userId", userById);
    router.get("/user/:userId", requireSignin, isAuth, read);
    router.get("/user/info/:userId", requireSignin, isAuth, info);
    router.get(
      "/secret/:userId",
      requireSignin,
      isAuth,
      isAdmin,
      (req, res) => {
        res.json({
          user: req.profile,
          auth: req.auth,
        });
      }
    );
  }
);

module.exports = router;
