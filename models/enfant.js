/* eslint-disable no-console */
const mongoose = require("mongoose");
const { ObjectId } = mongoose.Schema;
const enfantSchema = new mongoose.Schema(
  {
    nom: {
      type: String,
      required: true,
    },

    prenom: {
      type: String,
      required: true,
    },

    dateNaissance: {
      type: Date,
      required: true,
    },

    pere: {
      type: ObjectId,
      ref: "Employe",
    },

    mere: {
      type: ObjectId,
      ref: "Employe",
    },

    // name, price
  },
  { timestamps: true, typePojoToMixed: false }
);

module.exports = mongoose.model("Enfant", enfantSchema);
