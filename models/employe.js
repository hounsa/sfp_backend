/* eslint-disable no-console */
const mongoose = require("mongoose");

const { autoIncrement } = require("../connexion");

const { ObjectId } = mongoose.Schema;

const employeSchema = new mongoose.Schema(
  {
    id: Number,

    nom: {
      type: String,
      required: [true, "Vous devez définir un nom"],
    },

    prenom: {
      type: String,
      required: true,
    },

    nationalite: {
      type: String,
      required: [true, "Vous devez définir une nationnalité"],
    },

    cnssNumber: {
      type: Number,
      required: [true, "Vous devez définir le numéro cnss"],
    },
    cniNumber: {
      type: Number,
      required: [true, "Vous devez définir le numéro de la pièce d'identité"],
    },

    langues: {
      type: [String],
      required: [true, "Vous devez définir une langue parlée"],
    },

    dateNaissance: {
      type: String,
      required: true,
    },

    lieuNaissance: {
      type: String,
      required: true,
    },

    enfants: [
      {
        type: ObjectId,
        ref: "Enfant",
      },
    ],

    filiale: {
      type: ObjectId,
      ref: "Filiale",
    },
    createdBy: {
      type: ObjectId,
      ref: "User",
    },
  },
  { timestamps: true, typePojoToMixed: false }
);

employeSchema.plugin(autoIncrement.plugin, {
  model: "Employe",
  field: "id",
  startAt: 1,
});

module.exports = mongoose.model("Employe", employeSchema);
