const mongoose = require("mongoose");
const { ObjectId } = mongoose.Schema;

const filialeShema = new mongoose.Schema(
  {
    nom: { type: String, required: true },
    adresse: { type: String },
    phone: { type: String },
    createdBy: { type: ObjectId, ref: "User", required: true },
  },
  { timestamps: true }
);

module.exports = mongoose.model("Filiale", filialeShema);
