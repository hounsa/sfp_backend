const mongoose = require("mongoose");
const crypto = require("crypto");
const uuidv1 = require("uuid/v1");
const jwt = require("jsonwebtoken"); // to generate signed token
const expressJwt = require("express-jwt"); // for authaurization chek

const { ObjectId } = mongoose.Schema;

const userSchema = new mongoose.Schema(
  {
    userName: {
      type: String,
      trim: true,
      required: true,
      maxlength: 254,
    },

    employeId: {
      type: ObjectId,
      ref: "Employe",
    },

    email: {
      type: String,
      trim: true,
      required: true,
      unique: true,
    },
    hashed_password: {
      type: String,
      required: false,
    },
    isAdmin: Boolean,
    supUser: Boolean,
    salt: {
      type: String,
      required: false,
    },
  },
  { timestamps: true, typePojoToMixed: false }
);

// virtual field
userSchema
  .virtual("password")
  .set(function(password) {
    this._password = password;
    this.salt = uuidv1();
    this.hashed_password = this.encryptPassword(password);
  })
  .get(function() {
    return this._password;
  });

userSchema.methods = {
  // permet de verifier si le mot de pass entrer par l'utilisateur correspond à celui dans la base

  authenticate: function(plainText) {
    return this.encryptPassword(plainText) === this.hashed_password;
  },

  generateToken: function(res) {
    const payload = {
      email: this.email,
      username: this.username,
      _id: this._id,
    };

    let token = jwt.sign(payload, process.env.JWT_SECRET, {
      expiresIn: "24h",
    });

    res.cookie("jwt", token, { secure: true, httpOnly: true });
    return token;
  },

  encryptPassword: function(password) {
    if (!password) return "";

    try {
      const pass = crypto
        .createHmac("sha1", this.salt)
        .update(password)
        .digest("hex");

      return pass;
    } catch (err) {
      console.log({ err });
      return "";
    }
  },
};

module.exports = mongoose.model("User", userSchema);
