/* eslint-disable no-console */
const User = require("../models/user");
const { errorHandler } = require("../helpers/dbErrorHandler");

const { controllerHelper } = require("../utils/simpleControllerFactory");

const { update, byId, list } = controllerHelper(User, "user", true);

exports.userById = (req, res, next, id) => {
  User.findById(id).exec((err, user) => {
    if (err || !user) {
      return res.status(400).json({
        error: "User not found",
      });
    }
    req.profile = user;
    next();
  });
};

exports.read = (req, res) => {
  const { profile } = req;
  profile.hashed_password = undefined;
  profile.salt = undefined;
  return res.json(profile);
};

exports.info = async (req, res) => {
  try {
    return res.json(req.profile);
  } catch (error) {
    res.status(400).json({ error: "Une erreur s'est produite" });
  }
};

exports.update = (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const firstError = errors.array()[0].msg;
    return res.status(400).json({ error: firstError });
  }

  const { profile /*filiale*/ } = req;

  req.body.updateBy = profile;
  update(req, res, (filiale) =>
    res.json({
      filiale: filiale.depopulate("createdBy").depopulate("updatedBy"),
    })
  );
  // });
};

exports.list = list;
exports.remove = async (req, res) => {
  const { _id } = req.user;

  User.findOneAndRemove({ _id }).exec((error, result) => {
    if (error) {
      res.status(400).json({ error: errorHandler(error) });
    } else {
      res.json({ suucess: true, result });
    }
  });
};
