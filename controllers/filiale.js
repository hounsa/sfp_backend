/* eslint-disable no-console */
const Filiale = require("../models/filiale");
const { validationResult } = require("express-validator");
const { errorHandler } = require("../helpers/dbErrorHandler");
const { controllerHelper } = require("../utils/simpleControllerFactory");

const { create, update, byId, list } = controllerHelper(
  Filiale,
  "filiale",
  true
);

exports.create = (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const firstError = errors.array()[0].msg;
    return res.status(400).json({ error: firstError });
  }

  const { profile } = req;

  const newFiliale = req.body;
  const nom = newFiliale.nom.trim() || "";

  req.body.updatedBy = profile;
  req.body.createdBy = profile;
  req.body.nom = nom;

  create(req, res, (filiale) =>
    res.json({
      filiale,
    })
  );
};

exports.update = (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const firstError = errors.array()[0].msg;
    return res.status(400).json({ error: firstError });
  }

  const { profile /*filiale*/ } = req;

  req.body.updateBy = profile;
  update(req, res, (filiale) =>
    res.json({
      filiale: filiale.depopulate("createdBy").depopulate("updatedBy"),
    })
  );
  // });
};

exports.read = async (req, res) => {
  const { filiale } = req;
  await filiale.populate("createdBy").execPopulate();

  res.json(filiale);
};

exports.remove = async (req, res) => {
  const { _id } = req.filiale;

  Filiale.findOneAndRemove({ _id }).exec((error, result) => {
    if (error) {
      res.status(400).json({ error: errorHandler(error) });
    } else {
      res.json({ suucess: true, result });
    }
  });
};

exports.removeMany = async (req, res) => {
  const { ids } = req.body;

  Filiale.deleteMany({ _id: { $in: ids } }).exec((error, result) => {
    if (error) {
      res.status(400).json({ error: errorHandler(error) });
    } else {
      res.json({ success: true, result });
    }
  });
};

exports.byId = byId;
exports.list = list;
