/* eslint-disable no-console */

const User = require("../models/user");
const expressJwt = require("express-jwt");
const { validationResult } = require("express-validator");
const { errorHandler } = require("../helpers/dbErrorHandler");

exports.signup = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return rejectErros(res, errors);
  }

  try {
    const data = await saveUser(res, req.body);
    res.json({ ...data, success: true });
  } catch (error) {
    return rejectError(res, error, "in signup");
  }
};

const saveUser = (res, body) =>
  new Promise((resolve, reject) => {
    const userName = body.userName || `${body.firstName} ${body.lastName}`;
    const userBody = { ...body, userName };
    const user = new User(userBody);
    user.isAdmin = true;
    user.save(async (err, user) => {
      if (err) {
        return reject(err);
      }

      const token = user.generateToken(res);
      user.hashed_password = undefined;
      user.salt = undefined;
      resolve({ user, token });
    });
  });

exports.signin = async (req, res) => {
  const { email, password } = req.body;

  User.findOne({ email }, async (err, user) => {
    if (err || !user) {
      return res.status(401).json({
        error: "Email ou mot de pass invalide ",
        success: false,
      });
    }

    if (!user.authenticate(password)) {
      return res.status(401).json({
        error: "L'email ou mot de passe non valide",
        success: false,
      });
    }

    try {
      let token = user.generateToken(res);
      user.hashed_password = undefined;
      user.salt = undefined;
      res.status(200).json({ user, success: true, token });
    } catch (error) {
      rejectError(res, error, "in signin");
    }
  });
};

exports.signout = (req, res) => {
  res.clearCookie("jwt");
  res.json({ message: "Signout success" });
};

exports.requireSignin = expressJwt({
  secret: process.env.JWT_SECRET,
  requestProperty: "auth",
});

exports.isAuth = (req, res, next) => {
  let user = req.profile && req.auth && req.profile._id == req.auth._id;

  if (!user) {
    return res.status(401).json({
      error: "Access refusé",
    });
  }

  next();
};

exports.isAdmin = (req, res, next) => {
  const { profile } = req;

  if (!profile.isAdmin && !profile.supUser) {
    return res.status(401).json({
      error: "Droit d'accès backend! Accès refusé",
    });
  }
  next();
};

const rejectError = (res, error) => {
  // console.log(error);
  return res.status(400).json({
    error: errorHandler(error),
  });
};

const rejectErros = (res, errors) => {
  const firstError = errors.array()[0].msg;
  return res.status(400).json({ error: firstError });
};
