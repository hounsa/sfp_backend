/* eslint-disable no-console */
const Employe = require("../models/employe");
const { validationResult } = require("express-validator");
const { errorHandler } = require("../helpers/dbErrorHandler");
const { controllerHelper } = require("../utils/simpleControllerFactory");
const { builkEnfants, findByIds } = require("./enfant");
const { populate } = require("../models/employe");

const { byId } = controllerHelper(Employe, "employe", true);

exports.create = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const firstError = errors.array()[0].msg;
    return res.status(400).json({ error: firstError });
  }

  const { profile } = req;

  const newEmploye = req.body;
  const nom = newEmploye.nom.trim() || "";

  req.body.nom = nom;
  try {
    const employe = await performSave(req.body, req.profile);
    res.json({
      employe,
      success: true,
    });
  } catch (error) {
    res.status(400).json({
      error,
      success: false,
    });
  }
};

exports.update = async (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const firstError = errors.array()[0].msg;
    return res.status(400).json({ error: firstError });
  }

  for (let [key, value] of Object.entries(req.body)) {
    req.employe[key] = req.body[key];
  }

  try {
    const employe = await performSave(
      req.employe,
      req.profile,
      req.body.enfants
    );
    res.json({
      employe,
      success: true,
    });
  } catch (error) {
    res.status(400).json({
      error,
      success: false,
    });
  }
};

exports.read = async (req, res) => {
  const { employe } = req;

  await employe.execPopulate();
  await employe
    .populate({
      path: "enfants",
      select: "-createdAt  -updatedAt -__v",
    })
    .execPopulate();

  employe.__v = undefined;
  employe.updatedAt = undefined;
  employe.createdAt = undefined;
  employe.createdBy = undefined;

  res.json(employe);
};

exports.remove = async (req, res) => {
  const { _id } = req.employe;

  Employe.findOneAndRemove({ _id }).exec((error, result) => {
    if (error) {
      res.status(400).json({ error: errorHandler(error) });
    } else {
      res.json({ suucess: true, result });
    }
  });
};

exports.removeMany = async (req, res) => {
  const { ids } = req.body;

  Employe.deleteMany({ _id: { $in: ids } }).exec((error, result) => {
    if (error) {
      res.status(400).json({ error: errorHandler(error) });
    } else {
      res.json({ success: true, result });
    }
  });
};

const performSave = async (employe, profile, newEnfants = []) => {
  return new Promise(async (resolve, reject) => {
    const isUpdate = employe._id !== undefined;

    console.log({ newEnfants, employe });
    let enfants = (isUpdate ? newEnfants : employe.enfants) || [];

    let employeValue = employe;

    if (!isUpdate) {
      employeValue = new Employe(employe);
      employeValue.createdBy = profile;
    }

    let enfantsIds = [];

    if (enfants.length > 0) {
      try {
        await builkEnfants(enfants, profile).then(async (ids) => {
          enfantsIds = ids;
        });
      } catch (error) {
        console.log("save childreen in employe", error);
        reject(error);
      }
    }

    employeValue.enfants = enfantsIds;

    employeValue.save(async (err, value) => {
      if (err) {
        console.log({ err });
        return reject("something goes wrong");
      }

      resolve(value);
    });
  });
};

const list = (req, res) => {
  Employe.find()
    .sort("-updatedAt")
    .populate({
      path: "filiale",
      select: "-createdAt -updatedAt -createdBy -__v",
    })
    .select("-createdAt -updatedAt -createdBy")
    .exec((err, employes) => {
      if (err) {
        return res.status(400).json({
          error: errorHandler(err),
        });
      }
      res.json(employes);
    });
};

exports.byId = byId;
exports.list = list;
