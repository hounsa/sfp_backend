/* eslint-disable no-console */
const Enfant = require("../models/enfant");
const { validationResult } = require("express-validator");
const { errorHandler } = require("../helpers/dbErrorHandler");
const { controllerHelper } = require("../utils/simpleControllerFactory");
const { bulkUpdateModelValues } = require("../utils/index");

const { create, update, byId, list } = controllerHelper(Enfant, "enfant", true);

exports.create = (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const firstError = errors.array()[0].msg;
    return res.status(400).json({ error: firstError });
  }

  const { profile } = req;

  const newEnfant = req.body;
  const nom = newEnfant.nom.trim() || "";

  req.body.nom = nom;
  create(req, res, (enfant) =>
    res.json({
      enfant: enfant.depopulate("createdBy").depopulate("updatedBy"),
    })
  );
};

exports.update = (req, res) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    const firstError = errors.array()[0].msg;
    return res.status(400).json({ error: firstError });
  }

  const { profile /*enfant*/ } = req;

  req.body.updateBy = profile;
  update(req, res, (enfant) =>
    res.json({
      enfant: enfant.depopulate("createdBy").depopulate("updatedBy"),
    })
  );
  // });
};

exports.read = (req, res) => {
  const { enfant } = req;
  return res.json({ enfant: enfant });
};

exports.remove = async (req, res) => {
  const { _id } = req.body;

  Enfant.findOneAndRemove({ _id }).exec((error, result) => {
    if (error) {
      res.status(400).json({ error: errorHandler(error) });
    } else {
      res.json({ suucess: true, result });
    }
  });
};

exports.removeMany = async (req, res) => {
  const { ids } = req.body;

  Enfant.deleteMany({ _id: { $in: ids } }).exec((error, result) => {
    if (error) {
      res.status(400).json({ error: errorHandler(error) });
    } else {
      res.json({ success: true, result });
    }
  });
};

const builkEnfants = (enfantBody = [], profile) => {
  console.log({ enfantBody });

  return new Promise((resolve, reject) => {
    enfantBody.forEach((element) => {
      const nom = element.nom.trim() || "";
      const prenom = element.prenom.trim() || "";
      element.nom = nom;
      element.prenom = prenom;
    });

    bulkUpdateModelValues(Enfant, enfantBody)
      .then(async (ids) => {
        resolve(ids);
      })
      .catch((error) => {
        console.log({ error });
        reject(error);
      });
  });
};

const findByIds = async (ids) => {
  return new Promise((resolve, reject) => {
    Enfant.find({ _id: { $in: ids } }).exec((error, result) => {
      if (error) reject(error);
      resolve(result);
    });
  });
};

exports.builkEnfants = builkEnfants;
exports.findByIds = findByIds;
exports.byId = byId;
exports.list = list;
